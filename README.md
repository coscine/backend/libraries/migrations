## Migrations Library

[[_TOC_]] 

## 📝 Overview

The Migrations library for .NET simplifies SQL database migrations within Coscine by providing a set of migrations based on Fluent Migrator and an executable to start the migrations, enabling easy database schema updates and versioning while minimizing downtime and data loss.

## ⚙️ Configuration

To install this library, you can use the NuGet package manager or you can install it using the .NET CLI.
```powershell
Install-Package Coscine.Migrations
```
or using the .NET CLI
```powershell
dotnet add package Coscine.Migrations
```

## 📖 Usage

For examples on how we use this library, look into the source code of the following open-source [Coscine APIs](https://git.rwth-aachen.de/coscine/backend/apis) found in our public GitLab repository:
- `Project`
- `Resources`
... and others.

Our custom executable (Coscine.Migrator.exe) also allows for the creation of the needed database.

### Migrating Up (update)
Start a migration by calling `Coscine.Migrator.exe --migrate_up`.
This will create the needed database and run all migrations.
You can run the command again at a later time, to install additional migrations.
Migrations already deployed will not be deployed again.

### Migrating down (revert)
You can revert a migration by calling `Coscine.Migrator.exe --roll_back 1`.
This will remove the most recent migration.
Replacing the 1 with a 2 will revert the two most recent migrations and so on.

The executable will create a connection string based on the Consul values:
* DbDataSourceKey: `coscine/global/db_data_source`
* DbNameKey: `coscine/global/db_name`
* DbUserIdKey: `coscine/global/db_user_id`
* DbPasswordKey: `coscine/global/db_password`

### Other uses
You can call the migrations from a different project. The connection string information from Consul (except `db_name`) will be used.
Use the `CoscineMigrations` class for this:

```csharp
// Setup the migrator class
var targetAssembly = typeof(CoscineMigrations).Assembly;
var migrator = new CoscineMigrations();
var dbDatabase = "YOUR_DATABASE_NAME_HERE";
migrator.SetServiceProvider(targetAssembly, migrator.GetDatabaseConnectionStringFromConsul(dbDatabase));

// Create the database if needed
migrator.EnsureDatabase(dbDatabase);
// Run the migrations on the database
migrator.MigrateUp();
```

## 👥 Contributing

As an open source plattform and project, we welcome contributions from our community in any form. You can do so by submitting bug reports or feature requests, or by directly contributing to Coscine's source code. To submit your contribution please follow our [Contributing Guideline](https://git.rwth-aachen.de/coscine/docs/public/wiki/-/blob/master/Contributing%20To%20Coscine.md).

## 📄 License

The current open source repository is licensed under the **MIT License**, which is a permissive license that allows the software to be used, modified, and distributed for both commercial and non-commercial purposes, with limited restrictions (see `LICENSE` file)

> The MIT License allows for free use, modification, and distribution of the software and its associated documentation, subject to certain conditions. The license requires that the copyright notice and permission notice be included in all copies or substantial portions of the software. The software is provided "as is" without any warranties, and the authors or copyright holders cannot be held liable for any damages or other liability arising from its use.

## 🆘 Support

1. **Check the documentation**: Before reaching out for support, check the help pages provided by the team at https://docs.coscine.de/en/. This may have the information you need to solve the issue.
2. **Contact the team**: If the documentation does not help you or if you have a specific question, you can reach out to our support team at `servicedesk@itc.rwth-aachen.de` 📧. Provide a detailed description of the issue you're facing, including any error messages or screenshots if applicable.
3. **Be patient**: Our team will do their best to provide you with the support you need, but keep in mind that they may have a lot of requests to handle. Be patient and wait for their response.
4. **Provide feedback**: If the support provided by our support team helps you solve the issue, let us know! This will help us improve our documentation and support processes for future users.

By following these simple steps, you can get the support you need to use Coscine's services as an external user.

## 📦 Release & Changelog

External users can find the _Releases and Changelog_ inside each project's repository. The repository contains a section for Releases (`Deployments > Releases`), where users can find the latest release changelog and source. Withing the Changelog you can find a list of all the changes made in that particular release and version.  
By regularly checking for new releases and changes in the Changelog, you can stay up-to-date with the latest improvements and bug fixes by our team and community!