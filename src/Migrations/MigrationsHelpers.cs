﻿using System;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

namespace Coscine.Migrations
{
    public class MigrationsHelpers
    {
        public static void HandleInstitute(Action<object> action)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "Coscine.Migrations.Assets.Institutes.csv";
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (var reader = new StreamReader(stream))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (line.Contains(","))
                    {
                        var values = line.Split(',');
                        if (values[0].Trim() != "" && values[1].Trim() != "")
                        {
                            action.Invoke(new { IKZ = values[0].Trim(), DisplayName = values[1].Trim() });
                        }
                    }
                }
            }
        }

        public static void HandleDiscipline(Action<object> action)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "Coscine.Migrations.Assets.dfg_structure.rdf";
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (XmlReader reader = XmlReader.Create(stream))
            {
                reader.MoveToContent();
                XNamespace skosNameSpace = "http://www.w3.org/2004/02/skos/core#";
                XNamespace rdfNameSpace = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == "skos:Concept")
                        {
                            XElement el = (XElement)XNode.ReadFrom(reader);
                            var notation = el.Element(skosNameSpace + "notation");
                            if (int.TryParse(notation.Value, out int val) && notation.Value.Length == 3)
                            {
                                var url = el.Attribute(rdfNameSpace + "about").Value;
                                action.Invoke(new { DisplayName = el.Element(skosNameSpace + "prefLabel").Value, Url = url });
                            }
                        }
                    }
                }
            }
        }
    }
}
