﻿using Coscine.Configuration;
using Coscine.Database.Helpers;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Coscine.Migrations
{
    public class CoscineMigrations
    {
        private readonly IConfiguration _configuration;

        public ConnectionSettings ConnectionSettings { get; set; }

        public Assembly TargetAssembly { get; set; } = typeof(CoscineMigrations).Assembly;

        public CoscineMigrations()
        {
            _configuration = new ConsulConfiguration();
        }

        public CoscineMigrations(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ServiceProvider CreateServices()
        {
            return CreateServices(TargetAssembly, ConnectionSettings.GetConnectionString());
        }

        public ServiceProvider CreateServices(string dbConnectionString)
        {
            return CreateServices(TargetAssembly, dbConnectionString);
        }

        public ServiceProvider CreateServices(Assembly targetAssembly, string dbConnectionString)
        {
            return new ServiceCollection()
            // Registration of all FluentMigrator-specific services
            .AddFluentMigratorCore()
            // Configure the runner
            .ConfigureRunner(
                builder => builder
                    // Use SQLServer
                    .AddSqlServer()
                    // The SQLServer connection string  TODO: make configurable (via consul)
                    .WithGlobalConnectionString(dbConnectionString)
                    // Specify the assembly with the migrations
                    .ScanIn(targetAssembly).For.Migrations()
                    // Specify the assembly with the embedded resouces
                    .ScanIn(targetAssembly).For.EmbeddedResources())

            // Enable logging to console in the FluentMigrator way
            .AddLogging(lb => lb.AddFluentMigratorConsole())
            .BuildServiceProvider(false);
        }

        public void MigrateUp()
        {
            // Put the database update into a scope to ensure
            // that all resources will be disposed.
            using (var service = CreateServices())
            {
                using (var scope = service.CreateScope())
                {
                    // Instantiate the runner
                    var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();

                    // Execute the migrations
                    runner.MigrateUp();
                }
            }
        }

        public void RollBack(int steps)
        {
            using (var service = CreateServices())
            {
                using (var scope = CreateServices().CreateScope())
                {
                    var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
                    runner.Rollback(steps);
                }
            }
        }

        // Currently not in use
        public void MigrateDown(FluentMigrator.IMigration targetMigration)
        {
            using (var service = CreateServices())
            {
                using (var scope = CreateServices().CreateScope())
                {
                    var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
                    runner.Down(targetMigration);
                }
            }
        }

    }
}
