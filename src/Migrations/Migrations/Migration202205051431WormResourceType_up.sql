﻿DECLARE @rdsS3wormId AS uniqueidentifier
SELECT @rdsS3wormId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdss3worm';

INSERT INTO [ProjectQuotas]
           ([ProjectId]
           ,[ResourceTypeId]
           ,[Quota]
           ,[MaxQuota])

SELECT [Id], @rdsS3wormId, 0, 0
FROM [Projects]