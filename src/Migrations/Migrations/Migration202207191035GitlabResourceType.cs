﻿using Coscine.Configuration;
using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202207191035)]
    public class Migration202207191035GitlabResourceType : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey()
                .FromTable("ResourceDiscipline").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("ResourceDiscipline").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("ProjectResource").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("ProjectResource").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id");

            Execute.EmbeddedScript("Migration202207191035GitlabResourceType_down.sql");

            Update.Table("ResourceTypes")
                .Set(new { Type = "gitlab", SpecificType = "gitlab" })
                .Where(new { DisplayName = "gitlab" });

            // delete new table GitlabResourceType
            Delete.Table("GitlabResourceType");

            // create it like it was before
            Create.Table("GitlabResourceType")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("RepositoryNumber").AsInt32().NotNullable()
                .WithColumn("RepositoryUrl").AsString(500).NotNullable()
                .WithColumn("Token").AsString(100).NotNullable();
        }

        public override void Up()
        {
            Update.Table("ResourceTypes")
                .Set(new { Type = "gitlab", SpecificType = "gitlabrwth" })
                .Where(new { DisplayName = "gitlab" });

            Delete.ForeignKey()
                .FromTable("ResourceDiscipline").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("ResourceDiscipline").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id").OnDeleteOrUpdate(System.Data.Rule.Cascade);

            Delete.ForeignKey()
                .FromTable("ProjectResource").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("ProjectResource").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id").OnDeleteOrUpdate(System.Data.Rule.Cascade);

            // Add quotas and delete old gitlab entries from resources table
            Execute.EmbeddedScript("Migration202207191035GitlabResourceType_up.sql");

            // delete table GitlabResourceType
            Delete.Table("GitlabResourceType");

            // and create it new
            Create.Table("GitlabResourceType")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("Branch").AsString(200).NotNullable()
                .WithColumn("GitlabProjectId").AsInt32().NotNullable()
                .WithColumn("RepoUrl").AsString(200).NotNullable()
                .WithColumn("ProjectAccessToken").AsString(200).NotNullable();
        }
    }
}