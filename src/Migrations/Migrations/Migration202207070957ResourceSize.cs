﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202207070957)]
    public class Migration202207070957ResourceSize : Migration
    {
        public override void Down()
        {
            Alter.Table("RDSResourceType").AddColumn("Size").AsInt32().NotNullable().WithDefaultValue(0);
            Alter.Table("RdsS3ResourceType").AddColumn("Size").AsInt32().NotNullable().WithDefaultValue(0);
            Alter.Table("RdsS3WormResourceType").AddColumn("Size").AsInt32().NotNullable().WithDefaultValue(0);
        }

        public override void Up()
        {
            Delete.Column("Size").FromTable("RDSResourceType");
            Delete.Column("Size").FromTable("RdsS3ResourceType");
            Delete.Column("Size").FromTable("RdsS3WormResourceType");
        }
    }
}
