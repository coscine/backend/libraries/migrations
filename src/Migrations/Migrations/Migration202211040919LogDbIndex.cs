﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202211040919)]
    public class Migration202211040919LogDbIndex : Migration
    {
        public override void Down()
        {
            Delete.Index("log_idx_userid_loglevel_servertime").OnTable("Log");
        }

        public override void Up()
        {
            Create.Index("log_idx_userid_loglevel_servertime").OnTable("Log")
                .OnColumn("UserId").Ascending()
                .OnColumn("LogLevel").Ascending()
                .OnColumn("ServerTimestamp").Ascending();
        }
    }
}