DECLARE @rdsS3udeId AS uniqueidentifier
SELECT @rdsS3udeId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdss3ude';

DECLARE @rdsudeId AS uniqueidentifier
SELECT @rdsudeId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdsude';

DELETE FROM [dbo].[ProjectQuotas]
      WHERE [ResourceTypeId] = @rdsS3udeId OR [ResourceTypeId] = @rdsudeId