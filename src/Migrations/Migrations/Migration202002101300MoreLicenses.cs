﻿using FluentMigrator;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

#region DupFinder Exclusion
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202002101300)]
    public class Migration202002101300MoreLicenses : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.FromTable("Licenses").AllRows();
        }

        public override void Up()
        {
            HandleLicensesUp();
        }

        private void HandleLicensesUp()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "Coscine.Migrations.Assets.Licenses.rdf";
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (XmlReader reader = XmlReader.Create(stream))
            {
                reader.MoveToContent();
                XNamespace rdfNameSpace = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
                XNamespace rdfsNameSpace = "http://www.w3.org/2000/01/rdf-schema#";
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == "rdf:Description")
                        {
                            XElement el = (XElement)XNode.ReadFrom(reader);
                            var displayName = "";
                            var displayNames = el.Elements(rdfsNameSpace + "label");

                            foreach (var currentDisplayName in displayNames)
                            {
                                if (currentDisplayName.Attribute(XNamespace.Xml + "lang").Value.Equals("en"))
                                {
                                    displayName = currentDisplayName.Value;
                                }
                            }
                            Insert.IntoTable("Licenses").Row(new { DisplayName = displayName });
                        }
                    }
                }
            }

        }
    }
}
#endregion