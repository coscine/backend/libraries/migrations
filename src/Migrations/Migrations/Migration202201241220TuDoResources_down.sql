﻿DECLARE @rdsS3tudoId AS uniqueidentifier
SELECT @rdsS3tudoId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdss3tudo';

DECLARE @rdstudoId AS uniqueidentifier
SELECT @rdstudoId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdstudo';

DELETE FROM [dbo].[ProjectQuotas]
      WHERE [ResourceTypeId] = @rdsS3tudoId OR [ResourceTypeId] = @rdstudoId