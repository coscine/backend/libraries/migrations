﻿using Coscine.Configuration;
using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202201190837)]
    public class Migration202201190837NRWFHResources : Migration
    {
        public override void Down()
        {
            Execute.EmbeddedScript("Migration202201190837NRWFHResources_down.sql");

            Delete.FromTable("ResourceTypes").Row(new { DisplayName = "rdss3nrw" });
            Delete.FromTable("ResourceTypes").Row(new { DisplayName = "rdsnrw" });
        }

        public override void Up()
        {
            // Add resourcetypes
            Insert.IntoTable("ResourceTypes").Row(new { DisplayName = "rdss3nrw" });
            Insert.IntoTable("ResourceTypes").Row(new { DisplayName = "rdsnrw" });

            // Add quotas
            Execute.EmbeddedScript("Migration202201190837NRWFHResources_up.sql");
        }
    }
}
