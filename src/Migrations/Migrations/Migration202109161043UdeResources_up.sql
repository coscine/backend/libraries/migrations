DECLARE @rdsS3udeId AS uniqueidentifier
SELECT @rdsS3udeId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdss3ude';

DECLARE @rdsudeId AS uniqueidentifier
SELECT @rdsudeId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdsude';

INSERT INTO [ProjectQuotas]
           ([ProjectId]
           ,[ResourceTypeId]
           ,[Quota]
           ,[MaxQuota])

SELECT [Id], @rdsS3udeId, 0, 0
FROM [Projects]

INSERT INTO [ProjectQuotas]
           ([ProjectId]
           ,[ResourceTypeId]
           ,[Quota]
           ,[MaxQuota])

SELECT [Id], @rdsudeId, 0, 0
FROM [Projects]