﻿UPDATE ProjectQuotas
SET [Quota] = 0
WHERE [ResourceTypeId] = (
	SELECT RT.[Id]
	FROM ResourceTypes RT
	WHERE RT.[DisplayName] = 'rdss3'
);