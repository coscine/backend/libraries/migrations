DECLARE @rdsS3Id AS uniqueidentifier
SELECT @rdsS3Id = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdss3';

DECLARE @rdsId AS uniqueidentifier
SELECT @rdsId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rds';

UPDATE [dbo].[ProjectQuotas]
SET [MaxQuota] = 100
WHERE [ResourceTypeId] = @rdsId AND [Quota] > 0 AND [Quota] <= 100;

UPDATE [dbo].[ProjectQuotas]
SET [MaxQuota] = [Quota]
WHERE ([ResourceTypeId] = @rdsId OR [ResourceTypeId] = @rdsS3Id) AND [MaxQuota] < [Quota];
