﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202104281216)]
    public class Migration202104281216InvitationTabel : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Table("Invitations");
        }

        public override void Up()
        {
            Create.Table("Invitations")
                .WithColumn("Id").AsGuid().NotNullable().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("Project").AsGuid().NotNullable()
                .WithColumn("Issuer").AsGuid().NotNullable()
                .WithColumn("Role").AsGuid().NotNullable()
                .WithColumn("InviteeEmail").AsString(200).NotNullable()
                .WithColumn("Expiration").AsDateTime().NotNullable()
                .WithColumn("Token").AsGuid().NotNullable().WithDefault(SystemMethods.NewGuid);

            Create.ForeignKey()
                .FromTable("Invitations").ForeignColumn("Project")
                .ToTable("Projects").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("Invitations").ForeignColumn("Issuer")
                .ToTable("Users").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("Invitations").ForeignColumn("Role")
                .ToTable("Roles").PrimaryColumn("Id");
        }
    }
}
