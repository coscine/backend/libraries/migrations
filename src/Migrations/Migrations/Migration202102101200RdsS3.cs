﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202102101200)]
    public class Migration202102101200RdsS3 : Migration
    {
        public override void Down()
        {
            Execute.EmbeddedScript("Migration202102101200RdsS3_down.sql");
            Delete.FromTable("ResourceTypes").Row(new { DisplayName = "rdss3" });
            Delete.Table("RdsS3ResourceType");
        }

        public override void Up()
        {
            Create.Table("RdsS3ResourceType")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("BucketName").AsString(63).NotNullable()
                .WithColumn("AccessKey").AsString(200).NotNullable()
                .WithColumn("SecretKey").AsString(200).NotNullable()
                .WithColumn("AccessKeyRead").AsString(200).NotNullable()
                .WithColumn("SecretKeyRead").AsString(200).NotNullable()
                .WithColumn("AccessKeyWrite").AsString(200).NotNullable()
                .WithColumn("SecretKeyWrite").AsString(200).NotNullable()
                .WithColumn("Endpoint").AsString(200).NotNullable()
                .WithColumn("Size").AsInt32().NotNullable();

            Insert.IntoTable("ResourceTypes").Row(new { DisplayName = "rdss3" });

            Execute.EmbeddedScript("Migration202102101200RdsS3_up.sql");
        }
    }
}
