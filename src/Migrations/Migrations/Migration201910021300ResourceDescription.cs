﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201910021300)]
    public class Migration201910021300ResourceDescription : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Column("Description").FromTable("Resources");
        }

        public override void Up()
        {
            Alter.Table("Resources").AddColumn("Description").AsString(5000).Nullable();
        }
    }
}
