﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202104221020)]
    public class Migration202104221020RemoveQuotaDefault : Migration
    {
        public override void Down()
        {
            Alter.Column("Quota").OnTable("ProjectQuotas").AsInt32().NotNullable().WithDefaultValue(25);
        }

        public override void Up()
        {
            Alter.Column("Quota").OnTable("ProjectQuotas").AsInt32().NotNullable().WithDefaultValue(0);
        }
    }
}
