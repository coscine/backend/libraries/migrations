﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202104010900)]
    public class Migration202104010900RdsS3Fix : Migration
    {
        public override void Down()
        {
        }

        public override void Up()
        {
            Execute.EmbeddedScript("Migration202104010900RdsS3Fix_up.sql");
        }
    }
}
