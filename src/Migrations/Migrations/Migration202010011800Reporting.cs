﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202010011800)]
    public class Migration202010011800Reporting : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Table("Kpi");
        }

        public override void Up()
        {
            Create.Table("Kpi")
            .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
            .WithColumn("MeasurementID").AsString(500).Nullable()
            .WithColumn("Ikz").AsString(9).Nullable()
            .WithColumn("Value").AsDouble().Nullable()
            .WithColumn("Start").AsDateTime().WithDefault(SystemMethods.CurrentDateTime).Nullable()
            .WithColumn("End").AsDateTime().WithDefault(SystemMethods.CurrentDateTime).Nullable()
            .WithColumn("AdditionalInfo").AsString(500).WithDefaultValue("").Nullable()
            .WithColumn("AdditionalInfo1").AsString(500).WithDefaultValue("").Nullable()
            .WithColumn("AdditionalInfo2").AsString(500).WithDefaultValue("").Nullable()
            .WithColumn("AdditionalInfo3").AsString(500).WithDefaultValue("").Nullable()
            .WithColumn("AdditionalInfo4").AsString(500).WithDefaultValue("").Nullable()
            .WithColumn("AdditionalInfo5").AsString(500).WithDefaultValue("").Nullable()
            .WithColumn("SentSuccessfully").AsBoolean().WithDefaultValue(false);
        }
    }
}
