﻿UPDATE ExternalIds 
	SET Organization = 'https://orcid.org/'
	WHERE ExternalAuthenticatorId IN 
		(SELECT Id FROM ExternalAuthenticators WHERE 
			DisplayName = 'ORCiD');

UPDATE ExternalIds 
	SET Organization = 'https://login.rz.rwth-aachen.de/shibboleth'
	WHERE ExternalAuthenticatorId IN 
		(SELECT Id FROM ExternalAuthenticators WHERE 
			DisplayName = 'Shibboleth');