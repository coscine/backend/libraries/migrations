﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202011161100)]
    public class Migration202011161100ResourceTypeDefinition : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.FromTable("ResourceTypes").Row(new { DisplayName = "linked" });
            Delete.Table("LinkedResourceType");
        }

        public override void Up()
        {
            Insert.IntoTable("ResourceTypes").Row(new { DisplayName = "linked" });
            Create.Table("LinkedResourceType")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid);
        }
    }
}
