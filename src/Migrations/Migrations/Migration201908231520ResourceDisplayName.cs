﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201908231520)]
    public class Migration201908231520ResourceDisplayName : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Column("DisplayName").FromTable("Resources");
        }

        public override void Up()
        {
            Alter.Table("Resources").AddColumn("DisplayName").AsString(200).Nullable();
        }
    }
}
