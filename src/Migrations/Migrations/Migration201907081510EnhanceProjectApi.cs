﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201907081510)]
    public class Migration201907081510EnhanceProjectApi : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Column("StartDate").FromTable("Projects");
            Delete.Column("EndDate").FromTable("Projects");
            Delete.Column("Keywords").FromTable("Projects");
        }

        public override void Up()
        {
            Alter.Table("Projects")
                .AddColumn("StartDate").AsDateTime().WithDefault(SystemMethods.CurrentDateTime)
                .AddColumn("EndDate").AsDateTime()
                .AddColumn("Keywords").AsString(1000);
        }
    }
}
