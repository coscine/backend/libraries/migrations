﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202302271200)]
    public class Migration202302271200TopLevelProjectOrganizations : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("Migration202302271200TopLevelProjectOrganizations_up.sql");
        }
        public override void Down()
        {
            // I've added no down script since the change itself has no correct reversal.
        }
    }
}