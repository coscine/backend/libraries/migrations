﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201908290941)]
    public class Migration201908290941ORCiD : FluentMigrator.Migration
    {
        public override void Down()
        {
            Rename.Column("ExternalAuthenticatorId").OnTable("ExternalIds").To("ResourceTypeId");

            Delete.FromTable("ExternalAuthenticators").Row(new { DisplayName = "ORCiD" });
        }

        public override void Up()
        {
            Rename.Column("ResourceTypeId").OnTable("ExternalIds").To("ExternalAuthenticatorId");

            Insert.IntoTable("ExternalAuthenticators").Row(new { DisplayName = "ORCiD" });
        }
    }
}
