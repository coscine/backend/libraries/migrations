﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202208181027)]
    public class Migration202208181027ResourceDeletedColumn : Migration
    {
        public override void Down()
        {
            Delete.Column("Deleted")
                .FromTable("Resources");
        }

        public override void Up()
        {
            Alter.Table("Resources")
                .AddColumn("Deleted")
                .AsBoolean()
                .NotNullable()
                .WithDefaultValue(false);
        }
    }
}