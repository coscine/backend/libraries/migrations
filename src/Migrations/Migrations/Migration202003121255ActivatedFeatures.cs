﻿using FluentMigrator;
using System.Threading;

namespace Coscine.Migrations.Migrations
{   
    //yyyymmddhhmm
    [Migration(202003121255)]
    public class Migration202003121255ActivatedFeatures : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.ForeignKey()
                .FromTable("ActivatedFeatures").ForeignColumn("ProjectId")
                .ToTable("Projects").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("ActivatedFeatures").ForeignColumn("FeatureId")
                .ToTable("Features").PrimaryColumn("Id");

            Delete.Table("ActivatedFeatures");
            Delete.Table("Features");
        }

        public override void Up()
        {
            Create.Table("Features")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("SharepointId").AsString(200).NotNullable()
                .WithColumn("DisplaynameEn").AsString(200).NotNullable()
                .WithColumn("DisplaynameDe").AsString(200).NotNullable();

            Create.Table("ActivatedFeatures")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("ProjectId").AsGuid().NotNullable()
                .WithColumn("FeatureId").AsGuid().NotNullable();

            Create.ForeignKey()
                .FromTable("ActivatedFeatures").ForeignColumn("ProjectId")
                .ToTable("Projects").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("ActivatedFeatures").ForeignColumn("FeatureId")
                .ToTable("Features").PrimaryColumn("Id");


            Insert.IntoTable("Features").Row(new { SharepointId = "MSOZoneCell_WebPartWPQ2", DisplaynameEN = "Discussion Board", DisplaynameDe = "Discussion Board" });
            Insert.IntoTable("Features").Row(new { SharepointId = "MSOZoneCell_WebPartWPQ3", DisplaynameEN = "Documents", DisplaynameDe = "Dokumente" });
            Insert.IntoTable("Features").Row(new { SharepointId = "MSOZoneCell_WebPartWPQ4", DisplaynameEN = "Announcement Board", DisplaynameDe = "Announcement Board" });

            Execute.EmbeddedScript("Migration202003121255ActivatedFeatures_up.sql");
        }
    }
}
