﻿DECLARE @rdsS3nrwId AS uniqueidentifier
SELECT @rdsS3nrwId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdss3nrw';

DECLARE @rdsnrwId AS uniqueidentifier
SELECT @rdsnrwId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdsnrw';

INSERT INTO [ProjectQuotas]
           ([ProjectId]
           ,[ResourceTypeId]
           ,[Quota]
           ,[MaxQuota])

SELECT [Id], @rdsS3nrwId, 0, 0
FROM [Projects]

INSERT INTO [ProjectQuotas]
           ([ProjectId]
           ,[ResourceTypeId]
           ,[Quota]
           ,[MaxQuota])

SELECT [Id], @rdsnrwId, 0, 0
FROM [Projects]