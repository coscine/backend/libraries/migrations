﻿DECLARE @rdsS3nrwId AS uniqueidentifier
SELECT @rdsS3nrwId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdss3nrw';

DECLARE @rdsnrwId AS uniqueidentifier
SELECT @rdsnrwId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdsnrw';

DELETE FROM [dbo].[ProjectQuotas]
      WHERE [ResourceTypeId] = @rdsS3nrwId OR [ResourceTypeId] = @rdsnrwId