﻿using FluentMigrator;
using System;

#region DupFinder Exclusion
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202003231800)]
    public class Migration202003231800Organizations : FluentMigrator.Migration
    {
        public override void Down()
        {
            Create.Table("Institutes")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisplayName").AsString(200).NotNullable()
                .WithColumn("IKZ").AsString(20).NotNullable();

            MigrationsHelpers.HandleInstitute((obj) => Insert.IntoTable("Institutes").Row(obj));

            Delete.Column("OrganizationUrl").FromTable("ProjectInstitute");

            // Nullable because no better solution was found to add a foreign key relation without clearing the tables involved
            Alter.Table("ProjectInstitute").AddColumn("InstituteId").AsGuid().Nullable();             

            Create.ForeignKey()
                .FromTable("ProjectInstitute").ForeignColumn("InstituteId")
                .ToTable("Institutes").PrimaryColumn("Id");
        }

        public override void Up()
        {
            Delete.ForeignKey("FK_ProjectInstitute_InstituteId_Institutes_Id").OnTable("ProjectInstitute");
            Delete.Column("InstituteId").FromTable("ProjectInstitute");

            Delete.Table("Institutes");

            Alter.Table("ProjectInstitute")
                .AddColumn("OrganizationUrl")
                .AsString(255)
                .NotNullable()
                .WithDefaultValue("https://www.rwth-aachen.de/22000"); // Migration value for IT Center
        }
    }
}
#endregion
