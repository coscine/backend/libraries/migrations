﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202302271048)]
    public class Migration202302271048UniqueConstraintProjectQuotass : Migration
    {
        public override void Down()
        {
            Delete.UniqueConstraint("ProjectIdResourceTypeId").FromTable("ProjectQuotas").Columns("ProjectId", "ResourceTypeId");
        }

        public override void Up()
        {
            Create.UniqueConstraint("ProjectIdResourceTypeId").OnTable("ProjectQuotas").Columns("ProjectId", "ResourceTypeId");
        }
    }
}