﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202003192117)]
    public class Migration202003192117ORCiDEmailAddress : FluentMigrator.Migration
    {
        public override void Down()
        {
            Alter.Column("EMailAddress").OnTable("Users").AsString(200).NotNullable();
        }

        public override void Up()
        { 
            Alter.Column("EMailAddress").OnTable("Users").AsString(200).Nullable();
        }
    }

}
