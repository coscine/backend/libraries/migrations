﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201910241600)]
    public class Migration201910241600ResourceMetadata : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Column("ApplicationProfile").FromTable("Resources");
            Delete.Column("FixedValues").FromTable("Resources");
        }

        public override void Up()
        {
            Alter.Table("Resources").AddColumn("ApplicationProfile").AsString(500).Nullable();
            Alter.Table("Resources").AddColumn("FixedValues").AsString(5000).Nullable();
        }
    }
}
