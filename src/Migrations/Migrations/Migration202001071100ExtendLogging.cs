﻿using FluentMigrator;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202001071100)]
    public class Migration202001071100ExtendLogging : FluentMigrator.Migration
    {
        public override void Down()
        {
            Alter.Column("Message").OnTable("Log").AsString(500).Nullable();
        }

        public override void Up()
        {
            Alter.Column("Message").OnTable("Log").AsString(Int32.MaxValue).Nullable();
        }

    }
}