﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202101181145)]
    public class Migration202101181145DFNAAI : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Column("Organization").FromTable("ExternalIds");
        }

        public override void Up()
        {
            Alter.Table("ExternalIds").AddColumn("Organization").AsString().Nullable();

            Execute.EmbeddedScript("Migration202101181145DFNAAI_up.sql");
        }
    }
}
