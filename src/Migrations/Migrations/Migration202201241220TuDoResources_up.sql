﻿DECLARE @rdsS3tudoId AS uniqueidentifier
SELECT @rdsS3tudoId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdss3tudo';

DECLARE @rdstudoId AS uniqueidentifier
SELECT @rdstudoId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdstudo';

INSERT INTO [ProjectQuotas]
           ([ProjectId]
           ,[ResourceTypeId]
           ,[Quota]
           ,[MaxQuota])

SELECT [Id], @rdsS3tudoId, 0, 0
FROM [Projects]

INSERT INTO [ProjectQuotas]
           ([ProjectId]
           ,[ResourceTypeId]
           ,[Quota]
           ,[MaxQuota])

SELECT [Id], @rdstudoId, 0, 0
FROM [Projects]