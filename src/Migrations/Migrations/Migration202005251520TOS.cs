﻿using FluentMigrator;
using System;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202005251520)]
    public class Migration202005251520TOS : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Table("TOSAccepted");
        }

        public override void Up()
        {
            Create.Table("TOSAccepted")
                .WithColumn("RelationId").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("UserId").AsGuid().NotNullable()
                .WithColumn("Version").AsString(10).NotNullable();

            Create.ForeignKey()
                .FromTable("TOSAccepted").ForeignColumn("UserId")
                .ToTable("Users").PrimaryColumn("Id");
        }
    }
}
