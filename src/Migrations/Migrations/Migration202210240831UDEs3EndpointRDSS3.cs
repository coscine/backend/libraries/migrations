﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202210240831)]
    public class Migration202210240831UDEs3EndpointRDSS3 : Migration
    {
        public override void Down()
        {
            Update
                .Table("RdsS3ResourceType")
                .Set(new { Endpoint = "https://data-01-ecs-sh.fds.uni-due.de:9021" })
                .Where(new { Endpoint = "https://ecs-ude.fds.uni-due.de:443" });
        }

        public override void Up()
        {
            Update
                .Table("RdsS3ResourceType")
                .Set(new { Endpoint = "https://ecs-ude.fds.uni-due.de:443" })
                .Where(new { Endpoint = "https://data-01-ecs-sh.fds.uni-due.de:9021" });
        }
    }
}