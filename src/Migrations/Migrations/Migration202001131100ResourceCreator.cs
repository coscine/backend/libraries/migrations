﻿using FluentMigrator;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202001131100)]
    public class Migration202001131100ResourceCreator : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Column("Creator").FromTable("Resources");
        }

        public override void Up()
        {
            Alter.Table("Resources")
                .AddColumn("Creator").AsGuid().Nullable();
        }

    }
}