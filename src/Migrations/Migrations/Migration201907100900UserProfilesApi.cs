﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201907100900)]
    public class Migration201907100900UserProfilesApi : FluentMigrator.Migration
    {
        public override void Down()
        {
            #region Foreign Keys
            Delete.ForeignKey()
                .FromTable("GroupMemberships").ForeignColumn("UserId")
                .ToTable("Users").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("GroupMemberships").ForeignColumn("GroupId")
                .ToTable("Groups").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("ExternalIds").ForeignColumn("UserId")
                .ToTable("Users").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("ExternalIds").ForeignColumn("ResourceTypeId")
                .ToTable("ExternalAuthenticators").PrimaryColumn("Id");
            #endregion

            #region Tables
            Delete.Table("Groups");
            Delete.Table("GroupMemberships");
            Delete.Table("ExternalAuthenticators");
            Delete.Table("ExternalIds");
            #endregion

            #region Columns
            Delete.Column("EmailAddress").FromTable("Users");
            Delete.Column("DisplayName").FromTable("Users");
            #endregion
        }

        public override void Up()
        {
            #region Existing Tables
            Alter.Table("Users")
                .AddColumn("EmailAddress").AsString(255).NotNullable()
                .AddColumn("DisplayName").AsString(255).NotNullable();
            #endregion

            #region Independent Tables
            Create.Table("Groups")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisplayName").AsString(255).NotNullable();

            Create.Table("ExternalAuthenticators")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisplayName").AsString(50).NotNullable();
            #endregion

            #region GroupMemberships
            Create.Table("GroupMemberships")
                .WithColumn("RelationId").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("GroupId").AsGuid().NotNullable()
                .WithColumn("UserId").AsGuid().NotNullable();

            Create.ForeignKey()
                .FromTable("GroupMemberships").ForeignColumn("UserId")
                .ToTable("Users").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("GroupMemberships").ForeignColumn("GroupId")
                .ToTable("Groups").PrimaryColumn("Id");
            #endregion

            #region ExternalIds
            Create.Table("ExternalIds")
                .WithColumn("RelationId").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("UserId").AsGuid().NotNullable()
                .WithColumn("ResourceTypeId").AsGuid().NotNullable()
                .WithColumn("ExternalId").AsString(255);

            Create.ForeignKey()
                .FromTable("ExternalIds").ForeignColumn("UserId")
                .ToTable("Users").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("ExternalIds").ForeignColumn("ResourceTypeId")
                .ToTable("ExternalAuthenticators").PrimaryColumn("Id");
            #endregion
        }
    }
}
