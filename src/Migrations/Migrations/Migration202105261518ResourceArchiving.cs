﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.Migrations
{
    [Migration(202105261518)]
    public class Migration202105261518ResourceArchiving : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Column("Archived").FromTable("Resources");
        }

        public override void Up()
        {
            Alter.Table("Resources")
                .AddColumn("Archived").AsString().WithDefaultValue("0");
              
        }
    }
}
