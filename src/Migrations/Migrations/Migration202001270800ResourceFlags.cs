﻿using FluentMigrator;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202001270800)]
    public class Migration202001270800ResourceFlags : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Column("Enabled").FromTable("ResourceTypes");
        }

        public override void Up()
        {
            Alter.Table("ResourceTypes")
                .AddColumn("Enabled").AsBoolean().WithDefaultValue(true);
        }

    }
}