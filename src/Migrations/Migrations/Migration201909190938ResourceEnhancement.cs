﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201909190938)]
    public class Migration201909190938ResourceEnhancement : FluentMigrator.Migration
    {
        public override void Down()
        {
            #region Foreign Keys
            Delete.ForeignKey()
                .FromTable("ResourceDiscipline").ForeignColumn("DisciplineId")
                .ToTable("Disciplines").PrimaryColumn("Id");
            Delete.ForeignKey()
                .FromTable("ResourceDiscipline").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("Resources").ForeignColumn("VisibilityId")
                .ToTable("Visibilities").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("Resources").ForeignColumn("LicenseId")
                .ToTable("Licenses").PrimaryColumn("Id");
            #endregion

            Delete.Column("DisplayName").FromTable("Resources");

            Rename.Column("ResourceName").OnTable("Resources").To("DisplayName");

            Delete.Column("VisibilityId").FromTable("Resources");
            Delete.Column("LicenseId").FromTable("Resources");
            Delete.Column("Keywords").FromTable("Resources");
            Delete.Column("UsageRights").FromTable("Resources");
            Delete.Column("ResourceTypeOptionId").FromTable("Resources");

            Alter.Table("Resources")
                .AddColumn("Url").AsString(200).Nullable()
                .AddColumn("ExternalId").AsString(200).Nullable();

            Delete.Table("ResourceDiscipline");
            Delete.Table("Licenses");
            Delete.Table("RDSResourceType");
            Delete.Table("GitlabResourceType");
        }

        public override void Up()
        {
            Rename.Column("DisplayName").OnTable("Resources").To("ResourceName");

            Alter.Table("Resources").AddColumn("DisplayName").AsString(25).Nullable();

            Create.Table("ResourceDiscipline")
               .WithColumn("RelationId").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
               .WithColumn("DisciplineId").AsGuid().NotNullable()
               .WithColumn("ResourceId").AsGuid().NotNullable();

            Create.ForeignKey()
                .FromTable("ResourceDiscipline").ForeignColumn("DisciplineId")
                .ToTable("Disciplines").PrimaryColumn("Id");
            Create.ForeignKey()
                .FromTable("ResourceDiscipline").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id");

            Alter.Table("Resources").AddColumn("VisibilityId").AsGuid().Nullable();

            Create.ForeignKey()
                .FromTable("Resources").ForeignColumn("VisibilityId")
                .ToTable("Visibilities").PrimaryColumn("Id");

            Alter.Table("Resources").AddColumn("LicenseId").AsGuid().Nullable();

            Create.Table("Licenses")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisplayName").AsString(50).NotNullable();
            
            Create.ForeignKey()
                .FromTable("Resources").ForeignColumn("LicenseId")
                .ToTable("Licenses").PrimaryColumn("Id");

            Alter.Table("Resources").AddColumn("Keywords").AsString(1000).Nullable();
            Alter.Table("Resources").AddColumn("UsageRights").AsString(200).Nullable();

            Create.Table("RDSResourceType")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("BucketName").AsString(63).NotNullable()
                .WithColumn("AccessKey").AsString(200).Nullable()
                .WithColumn("SecretKey").AsString(200).Nullable();

            Create.Table("GitlabResourceType")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("RepositoryNumber").AsInt32().NotNullable()
                .WithColumn("RepositoryUrl").AsString(500).NotNullable()
                .WithColumn("Token").AsString(100).NotNullable();
             
            Alter.Table("Resources").AddColumn("ResourceTypeOptionId").AsGuid().Nullable();
            
            Delete.Column("ExternalId").FromTable("Resources");
            Delete.Column("Url").FromTable("Resources");
        }
    }
}
