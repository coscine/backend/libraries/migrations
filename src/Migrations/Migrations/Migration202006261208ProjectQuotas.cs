﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202006261208)]
    public class Migration202006261208ProjectQuotas : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Table("ProjectQuotas");
        }

        public override void Up()
        {
            Create.Table("ProjectQuotas")
                .WithColumn("RelationId").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("ProjectId").AsGuid().NotNullable()
                .WithColumn("ResourceTypeId").AsGuid().NotNullable()
                .WithColumn("Quota").AsInt32().NotNullable().WithDefaultValue(25);

            Create.ForeignKey()
                .FromTable("ProjectQuotas").ForeignColumn("ProjectId")
                .ToTable("Projects").PrimaryColumn("Id");
            Create.ForeignKey()
                .FromTable("ProjectQuotas").ForeignColumn("ResourceTypeId")
                .ToTable("ResourceTypes").PrimaryColumn("Id");

            Execute.EmbeddedScript("Migration202006261208ProjectQuotas_up.sql");
        }
    }
}
