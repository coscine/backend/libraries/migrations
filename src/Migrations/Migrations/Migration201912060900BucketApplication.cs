﻿using FluentMigrator;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

#region DupFinder Exclusion
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201912060900)]
    public class Migration201912060900BucketApplication : FluentMigrator.Migration
    {
        public override void Down()
        {
            // update the resource type table
            Delete.FromTable("ResourceTypes").Row(new { DisplayName = "rds" });
            Update.Table("ResourceTypes").Set(new { DisplayName = "rds" }).Where(new { DisplayName = "s3" });

            // deletion of the table
            Delete.Table("RDSResourceType");

            // renaming of the s3resource table
            Rename.Table("S3ResourceType").To("RDSResourceType");
            Execute.Sql("EXEC sp_rename 'DF_S3ResourceType_Id','DF_RDSResourceType_Id', 'object'");
            Execute.Sql("EXEC sp_rename N'PK_S3ResourceType', 'PK_RDSResourceType', 'object';");
        }


        public override void Up()
        {
            // update the resource type table
            Update.Table("ResourceTypes").Set(new { DisplayName = "s3" }).Where(new { DisplayName = "rds" });
            Insert.IntoTable("ResourceTypes").Row(new { DisplayName = "rds" });

            // renaming of the rdsresource table
            Rename.Table("RDSResourceType").To("S3ResourceType");
            Execute.Sql("EXEC sp_rename 'DF_RDSResourceType_Id','DF_S3ResourceType_Id', 'object'");
            Execute.Sql("EXEC sp_rename N'PK_RDSResourceType', 'PK_S3ResourceType', 'object';");

            // creation of the new table
            Create.Table("RDSResourceType")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("BucketName").AsString(63).NotNullable()
                .WithColumn("Size").AsInt32().Nullable();
        }

    }
}
#endregion