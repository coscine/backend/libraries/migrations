﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202010271100)]
    public class Migration202010271100ApiTokens : Migration
    {
        public override void Down()
        {
            Delete.Table("ApiTokens");
        }

        public override void Up()
        {
            Create.Table("ApiTokens")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("UserId").AsGuid().NotNullable()
                .WithColumn("IssuedAt").AsDateTime().NotNullable()
                .WithColumn("Expiration").AsDateTime().NotNullable();

            Create.ForeignKey()
                .FromTable("ApiTokens").ForeignColumn("UserId")
                .ToTable("Users").PrimaryColumn("Id");
        }
    }
}
