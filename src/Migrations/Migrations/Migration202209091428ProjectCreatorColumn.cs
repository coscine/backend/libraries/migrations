﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202209091428)]
    public class Migration202209091428ProjectCreatorColumn : Migration
    {
        public override void Down()
        {
            Delete
                .Column("Creator")
                .FromTable("Projects");
        }

        public override void Up()
        {
            Alter
                .Table("Projects")
                .AddColumn("Creator")
                .AsGuid()
                .Nullable();
        }
    }
}