﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202205051431)]
    public class Migration202205051431WormResourceType : Migration
    {
        public override void Down()
        {
            Execute.EmbeddedScript("Migration202205051431WormResourceType_down.sql");

            Delete.FromTable("ResourceTypes").Row(new { DisplayName = "rdss3worm" });

            Delete.Column("Type").FromTable("ResourceTypes");
            Delete.Column("SpecificType").FromTable("ResourceTypes");
            Delete.Table("RdsS3WormResourceType");
        }

        public override void Up()
        {
            Alter.Table("ResourceTypes")
                .AddColumn("Type").AsString(200).Nullable()
                .AddColumn("SpecificType").AsString(200).Nullable();

            Update.Table("ResourceTypes")
                .Set(new { Type = "rds", SpecificType = "rdstudo" })
                .Where(new { DisplayName = "rdstudo" });

            Update.Table("ResourceTypes")
                .Set(new { Type = "rds", SpecificType = "rdsrwth" })
                .Where(new { DisplayName = "rds" });

            Update.Table("ResourceTypes")
                .Set(new { Type = "rdss3", SpecificType = "rdss3nrw" })
                .Where(new { DisplayName = "rdss3nrw" });

            Update.Table("ResourceTypes")
                .Set(new { Type = "gitlab", SpecificType = "gitlab" })
                .Where(new { DisplayName = "gitlab" });

            Update.Table("ResourceTypes")
                .Set(new { Type = "rds", SpecificType = "rdsude" })
                .Where(new { DisplayName = "rdsude" });

            Update.Table("ResourceTypes")
                .Set(new { Type = "s3", SpecificType = "s3" })
                .Where(new { DisplayName = "s3" });

            Update.Table("ResourceTypes")
                .Set(new { Type = "rdss3", SpecificType = "rdss3ude" })
                .Where(new { DisplayName = "rdss3ude" });

            Update.Table("ResourceTypes")
                .Set(new { Type = "linked", SpecificType = "linked" })
                .Where(new { DisplayName = "linked" });

            Update.Table("ResourceTypes")
                .Set(new { Type = "rdss3", SpecificType = "rdss3tudo" })
                .Where(new { DisplayName = "rdss3tudo" });

            Update.Table("ResourceTypes")
               .Set(new { Type = "rds", SpecificType = "rdsnrw" })
               .Where(new { DisplayName = "rdsnrw" });

            Update.Table("ResourceTypes")
               .Set(new { Type = "rdss3", SpecificType = "rdss3rwth" })
               .Where(new { DisplayName = "rdss3" });

            Insert.IntoTable("ResourceTypes")
                .Row(new { DisplayName = "rdss3worm", Type = "rdss3worm", SpecificType = "rdss3wormrwth" });

            Create.Table("RdsS3WormResourceType")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("BucketName").AsString(63).NotNullable()
                .WithColumn("AccessKey").AsString(200).NotNullable()
                .WithColumn("SecretKey").AsString(200).NotNullable()
                .WithColumn("AccessKeyRead").AsString(200).NotNullable()
                .WithColumn("SecretKeyRead").AsString(200).NotNullable()
                .WithColumn("AccessKeyWrite").AsString(200).NotNullable()
                .WithColumn("SecretKeyWrite").AsString(200).NotNullable()
                .WithColumn("Endpoint").AsString(200).NotNullable()
                .WithColumn("Size").AsInt32().NotNullable();

            Execute.EmbeddedScript("Migration202205051431WormResourceType_up.sql");
        }
    }
}