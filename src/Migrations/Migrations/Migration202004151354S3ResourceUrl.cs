﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202004151354)]
    public class Migration202004151354S3ResourceUrl : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Column("ResourceUrl").FromTable("S3ResourceType");
        }

        public override void Up()
        {
            Alter.Table("S3ResourceType").AddColumn("ResourceUrl").AsString(200).Nullable();
            Update.Table("S3ResourceType").Set(new { ResourceUrl = "https://s3.rwth-aachen.de" }).AllRows();
            Alter.Table("S3ResourceType").AlterColumn("ResourceUrl").AsString(200).NotNullable();
        }
    }
}
