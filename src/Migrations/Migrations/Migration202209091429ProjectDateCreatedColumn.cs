﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202209091429)]
    public class Migration202209091429ProjectDateCreatedColumn : Migration
    {
        public override void Down()
        {
            Delete
                .Column("DateCreated")
                .FromTable("Projects");
        }

        public override void Up()
        {
            Alter
                .Table("Projects")
                .AddColumn("DateCreated")
                .AsDateTime()
                .WithDefault(SystemMethods.CurrentUTCDateTime)
                .Nullable();
        }
    }
}