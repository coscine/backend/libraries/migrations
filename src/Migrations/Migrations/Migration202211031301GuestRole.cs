﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202211031301)]
    public class Migration202211031301GuestRole : Migration
    {
        public override void Down()
        {
            Delete.FromTable("Roles").Row(new { DisplayName = "Guest", Description = "Guest of the project." });
        }

        public override void Up()
        {
            Insert.IntoTable("Roles").Row(new { DisplayName = "Guest", Description = "Guest of the project." });
        }
    }
}