﻿INSERT INTO ProjectQuotas ([ProjectId], [ResourceTypeId])
(
	SELECT P.[Id], RT.[Id]
	FROM Projects P, ResourceTypes RT
	WHERE RT.[DisplayName] = 'rdss3'
);