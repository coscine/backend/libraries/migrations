﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202211080726)]
    public class Migration202211080726GitlabTosAcceptedColumn : Migration
    {
        public override void Down()
        {
            Delete.Column("TosAccepted")
                .FromTable("GitlabResourceType");
        }

        public override void Up()
        {
            Alter.Table("GitlabResourceType")
                .AddColumn("TosAccepted")
                .AsBoolean()
                .NotNullable()
                .WithDefaultValue(false);
        }
    }
}