﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202209270927)]
    public class Migration202209270927MetadataExtractionTable : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey()
                .FromTable("MetadataExtraction").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id");

            Delete
                .Table("MetadataExtraction");
        }

        public override void Up()
        {
            Create
                .Table("MetadataExtraction")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("ResourceId").AsGuid().NotNullable()
                .WithColumn("Activated").AsBoolean().WithDefaultValue(false);

            Create.ForeignKey()
                .FromTable("MetadataExtraction").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id").OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}