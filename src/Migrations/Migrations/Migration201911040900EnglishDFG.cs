﻿using FluentMigrator;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

#region DupFinder Exclusion
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201911040900)]
    public class Migration201911040900EnglishDFG : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Column("DisplayNameDe").FromTable("Disciplines");
            Delete.Column("DisplayNameEn").FromTable("Disciplines");
            Alter.Table("Disciplines").AddColumn("DisplayName").AsString(200).Nullable();
            HandleDisciplineEnDown();
        }

        public override void Up()
        {
            Delete.Column("DisplayName").FromTable("Disciplines");
            Alter.Table("Disciplines").AddColumn("DisplayNameDe").AsString(200).Nullable();
            Alter.Table("Disciplines").AddColumn("DisplayNameEn").AsString(200).Nullable();
            HandleDisciplineEnUp();
        }

        private void HandleDisciplineEnUp()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "Coscine.Migrations.Assets.dfg_structure.rdf";
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (XmlReader reader = XmlReader.Create(stream))
            {
                reader.MoveToContent();
                XNamespace skosNameSpace = "http://www.w3.org/2004/02/skos/core#";
                XNamespace rdfNameSpace = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == "skos:Concept")
                        {
                            XElement el = (XElement) XNode.ReadFrom(reader);
                            var notation = el.Element(skosNameSpace + "notation");
                            if (int.TryParse(notation.Value, out int val) && notation.Value.Length == 3)
                            {
                                var url = el.Attribute(rdfNameSpace + "about").Value;
                                var digits = el.Element(skosNameSpace + "notation").Value;
                                var displayNames = el.Elements(skosNameSpace + "prefLabel");
                                var displayNameDe = "Keine Angabe" + " " + digits;
                                var displayNameEn = "No Information" + " " + digits;

                                foreach (var displayName in displayNames)
                                {
                                   if (displayName.Attribute(XNamespace.Xml + "lang").Value.Equals("de"))
                                   {
                                        displayNameDe = displayName.Value + " " + digits;
                                   }
                                   else if (displayName.Attribute(XNamespace.Xml + "lang").Value.Equals("en"))
                                   {
                                        displayNameEn = displayName.Value + " " + digits;
                                   }
                                }

                                Update.Table("Disciplines").Set(new { DisplayNameDe = displayNameDe, DisplayNameEn = displayNameEn }).Where(new { Url = url });
                            }
                        }
                    }
                }
            }

        }


        private void HandleDisciplineEnDown()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "Coscine.Database.Migration.Assets.dfg_structure.rdf";
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (XmlReader reader = XmlReader.Create(stream))
            {
                reader.MoveToContent();
                XNamespace skosNameSpace = "http://www.w3.org/2004/02/skos/core#";
                XNamespace rdfNameSpace = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == "skos:Concept")
                        {
                            XElement el = (XElement)XNode.ReadFrom(reader);
                            var notation = el.Element(skosNameSpace + "notation");
                            if (int.TryParse(notation.Value, out int val) && notation.Value.Length == 3)
                            {
                                var url = el.Attribute(rdfNameSpace + "about").Value;
                                var digits = el.Element(skosNameSpace + "notation").Value;
                                var displayNames = el.Elements(skosNameSpace + "prefLabel");
                                var displayNameDe = "Keine Angabe";

                                foreach(var displayName in displayNames)
                                {
                                    if (displayName.Attribute(XNamespace.Xml + "lang").Value.Equals("de"))
                                    {
                                        displayNameDe = displayName.Value;
                                    }
                                }
                                Update.Table("Disciplines").Set(new { DisplayName = displayNameDe }).Where(new { Url = url });
                            }
                        }
                    }
                }
            }

        }
    }
}
#endregion