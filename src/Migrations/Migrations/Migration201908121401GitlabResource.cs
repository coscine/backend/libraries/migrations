﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201908121401)]
    public class Migration201908121401GitlabResource : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.FromTable("ResourceTypes").Row(new { DisplayName = "gitlab" });
        }

        public override void Up()
        {
            Insert.IntoTable("ResourceTypes").Row(new { DisplayName = "gitlab" });
        }
    }
}
