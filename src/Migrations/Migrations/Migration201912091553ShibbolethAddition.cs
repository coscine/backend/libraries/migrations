﻿using FluentMigrator;


#region DupFinder Exclusion
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201912091553)]
    public class Migration201912091553ShibbolethAddition : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.FromTable("ExternalAuthenticators").Row(new { DisplayName = "Shibboleth" });

            Delete.Column("Entitlement").FromTable("Users");
            Delete.Column("Organization").FromTable("Users");
        }

        public override void Up()
        {
            Insert.IntoTable("ExternalAuthenticators").Row(new { DisplayName = "Shibboleth" });

            Alter.Table("Users").AddColumn("Entitlement").AsString(200).Nullable();
            Alter.Table("Users").AddColumn("Organization").AsString(200).Nullable();
        }
    }
}
#endregion