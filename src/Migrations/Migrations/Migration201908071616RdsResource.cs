﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201908071616)]
    public class Migration201908071616RdsResource : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.FromTable("ResourceTypes").Row(new { DisplayName = "rds" });
        }

        public override void Up()
        {

            Insert.IntoTable("ResourceTypes").Row(new { DisplayName = "rds" });
        }
    }
}
