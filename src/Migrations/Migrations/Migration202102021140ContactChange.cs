﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202102021140)]
    public class Migration202102021140ContactChange : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Table("ContactChange");
        }

        public override void Up()
        {
            Create.Table("ContactChange")
                .WithColumn("RelationId").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("UserId").AsGuid()
                .WithColumn("NewEmail").AsString(200).NotNullable()
                .WithColumn("EditDate").AsDateTime().Nullable()
                .WithColumn("ConfirmationToken").AsGuid().NotNullable().WithDefault(SystemMethods.NewGuid);

            Create.ForeignKey()
                .FromTable("ContactChange").ForeignColumn("UserId")
                .ToTable("Users").PrimaryColumn("Id");
        }
    }
}
