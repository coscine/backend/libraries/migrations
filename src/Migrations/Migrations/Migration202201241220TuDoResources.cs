﻿using Coscine.Configuration;
using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202201241220)]
    public class Migration202201241220TuDoResources : Migration
    {
        public override void Down()
        {
            Execute.EmbeddedScript("Migration202201241220TuDoResources_down.sql");

            Delete.FromTable("ResourceTypes").Row(new { DisplayName = "rdss3tudo" });
            Delete.FromTable("ResourceTypes").Row(new { DisplayName = "rdstudo" });
        }

        public override void Up()
        {
            // Add resourcetypes
            Insert.IntoTable("ResourceTypes").Row(new { DisplayName = "rdss3tudo" });
            Insert.IntoTable("ResourceTypes").Row(new { DisplayName = "rdstudo" });

            // Add quotas
            Execute.EmbeddedScript("Migration202201241220TuDoResources_up.sql");
        }
    }
}
