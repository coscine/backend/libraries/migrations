﻿DECLARE @gitlabId AS uniqueidentifier
SELECT @gitlabId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'gitlab';

DELETE FROM [dbo].[ProjectQuotas]
WHERE [ResourceTypeId] = @gitlabId;

-- delete gitlab entries from resources
DELETE FROM [dbo].[Resources]
WHERE [TypeId] = @gitlabId;