﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202210180955)]
    public class Migration202210180955GitlabResourceRenamed : Migration
    {
        public override void Down()
        {
            Update
                .Table("ResourceTypes")
                .Set(new { SpecificType = "gitlabrwth" })
                .Where(new { SpecificType = "gitlab" });
        }

        public override void Up()
        {
            Update
                .Table("ResourceTypes")
                .Set(new { SpecificType = "gitlab" })
                .Where(new { SpecificType = "gitlabrwth" });
        }
    }
}