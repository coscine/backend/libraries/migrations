﻿DELETE FROM [dbo].[ProjectQuotas] WHERE [RelationId] IN (
    SELECT [RelationId] FROM (
        SELECT 
            [RelationId]
            , Count(*) OVER (PARTITION BY [ProjectId], [ResourceTypeId] ORDER BY [MaxQuota] DESC) AS [quantity]
        FROM 
            [dbo].[ProjectQuotas]
    ) a WHERE [quantity] > 1 
)