﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202209091629)]
    public class Migration202209091629ResourceDateCreatedColumn : Migration
    {
        public override void Down()
        {
            Delete
                .Column("DateCreated")
                .FromTable("Resources");
        }

        public override void Up()
        {
            Alter
                .Table("Resources")
                .AddColumn("DateCreated")
                .AsDateTime()
                .WithDefault(SystemMethods.CurrentUTCDateTime)
                .Nullable();
        }
    }
}