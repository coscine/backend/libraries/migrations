﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201909111125)]
    public class Migration201909111125ProjectEnhancement : FluentMigrator.Migration
    {
        public override void Down()
        {
            #region Foreign Keys
            Delete.ForeignKey()
                .FromTable("ProjectDiscipline").ForeignColumn("DisciplineId")
                .ToTable("Disciplines").PrimaryColumn("Id");
            Delete.ForeignKey()
                .FromTable("ProjectDiscipline").ForeignColumn("ProjectId")
                .ToTable("Projects").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("ProjectInstitute").ForeignColumn("InstituteId")
                .ToTable("Institutes").PrimaryColumn("Id");
            Delete.ForeignKey()
                .FromTable("ProjectInstitute").ForeignColumn("ProjectId")
                .ToTable("Projects").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("Projects").ForeignColumn("VisibilityId")
                .ToTable("Visibilities").PrimaryColumn("Id");
            #endregion
            
            Delete.Column("DisplayName").FromTable("Projects");
            Delete.Column("PrincipleInvestigators").FromTable("Projects");
            Delete.Column("GrantId").FromTable("Projects");

            Alter.Table("Projects").AlterColumn("Description").AsString(1000).NotNullable();

            Rename.Column("ProjectName").OnTable("Projects").To("DisplayName");

            Delete.Column("VisibilityId").FromTable("Projects");

            Delete.Table("ProjectDiscipline");
            Delete.Table("Disciplines");
            Delete.Table("ProjectInstitute");
            Delete.Table("Institutes");
            Delete.Table("Visibilities");

            Alter.Table("Projects").AddColumn("Organization").AsString(50).Nullable();
        }

        public override void Up()
        {
            Rename.Column("DisplayName").OnTable("Projects").To("ProjectName");

            Delete.Column("Organization").FromTable("Projects");

            Alter.Table("Projects").AlterColumn("Description").AsString(5000).NotNullable();

            Alter.Table("Projects").AddColumn("DisplayName").AsString(25).Nullable();
            Alter.Table("Projects").AddColumn("PrincipleInvestigators").AsString(500).Nullable();
            Alter.Table("Projects").AddColumn("GrantId").AsString(500).Nullable();

            Create.Table("Disciplines")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisplayName").AsString(200).NotNullable()
                .WithColumn("Url").AsString(200).NotNullable();

            Create.Table("ProjectDiscipline")
                .WithColumn("RelationId").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisciplineId").AsGuid().NotNullable()
                .WithColumn("ProjectId").AsGuid().NotNullable();

            Create.ForeignKey()
                .FromTable("ProjectDiscipline").ForeignColumn("DisciplineId")
                .ToTable("Disciplines").PrimaryColumn("Id");
            Create.ForeignKey()
                .FromTable("ProjectDiscipline").ForeignColumn("ProjectId")
                .ToTable("Projects").PrimaryColumn("Id");

            Create.Table("Institutes")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisplayName").AsString(200).NotNullable()
                .WithColumn("IKZ").AsString(20).NotNullable();

            Create.Table("ProjectInstitute")
                .WithColumn("RelationId").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("InstituteId").AsGuid().NotNullable()
                .WithColumn("ProjectId").AsGuid().NotNullable();

            Create.ForeignKey()
                .FromTable("ProjectInstitute").ForeignColumn("InstituteId")
                .ToTable("Institutes").PrimaryColumn("Id");
            Create.ForeignKey()
                .FromTable("ProjectInstitute").ForeignColumn("ProjectId")
                .ToTable("Projects").PrimaryColumn("Id");

            Alter.Table("Projects").AddColumn("VisibilityId").AsGuid().Nullable();

            Create.Table("Visibilities")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisplayName").AsString(50).NotNullable();

            Insert.IntoTable("Visibilities").Row(new { DisplayName = "Project Members" });
            Insert.IntoTable("Visibilities").Row(new { DisplayName = "Public" });

            Create.ForeignKey()
                .FromTable("Projects").ForeignColumn("VisibilityId")
                .ToTable("Visibilities").PrimaryColumn("Id");

            Alter.Column("Keywords").OnTable("Projects").AsString(1000).Nullable();
        }
    }
}
