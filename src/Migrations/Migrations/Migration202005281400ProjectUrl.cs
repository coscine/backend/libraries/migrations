﻿using FluentMigrator;
using System;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202005281400)]
    public class Migration202005281400ProjectUrl : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Column("Slug").FromTable("Projects");
        }

        public override void Up()
        {
            Alter.Table("Projects").AddColumn("Slug").AsString(63).Nullable();
            Execute.EmbeddedScript("Migration202005281400ProjectUrl_up.sql");
            Alter.Table("Projects").AlterColumn("Slug").AsString(63).NotNullable();
        }
    }
}
