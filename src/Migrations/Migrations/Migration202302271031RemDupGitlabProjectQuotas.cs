﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202302271031)]
    public class Migration202302271031RemDupGitlabProjectQuotas : Migration
    {
        public override void Down()
        {
            
        }

        public override void Up()
        {
            Execute.EmbeddedScript("Migration202302271031RemDupGitlabProjectQuotas_up.sql");
        }
    }
}