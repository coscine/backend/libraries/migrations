﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202106091057)]
    public class Migration202106091057QuotaColumn : Migration
    {
        public override void Down()
        {
            Delete.Column("MaxQuota").FromTable("ProjectQuotas");
        }

        public override void Up()
        {
            Alter.Table("ProjectQuotas")
                .AddColumn("MaxQuota").AsInt32().WithDefaultValue("0").NotNullable();

            Execute.EmbeddedScript("Migration202106091057QuotaColumn_up.sql");
        }
    }
}
