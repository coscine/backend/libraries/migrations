﻿using Coscine.Configuration;
using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202109161043)]
    public class Migration202109161043UdeResources : Migration
    {
        public override void Down()
        {
            Execute.EmbeddedScript("Migration202109161043UdeResources_down.sql");

            Delete.Column("AccessKey").FromTable("RDSResourceType");
            Delete.Column("SecretKey").FromTable("RDSResourceType");
            Delete.Column("Endpoint").FromTable("RDSResourceType");

            Delete.FromTable("ResourceTypes").Row(new { DisplayName = "rdss3ude" });
            Delete.FromTable("ResourceTypes").Row(new { DisplayName = "rdsude" });
        }

        public override void Up()
        {
            var configuration = new ConsulConfiguration();

            // Make nullable, as we extend the table, which already has rows
            Alter.Table("RDSResourceType")
                .AddColumn("AccessKey").AsString(200).Nullable()
                .AddColumn("SecretKey").AsString(200).Nullable()
                .AddColumn("Endpoint").AsString(200).Nullable();

            // Insert missing data
            Update.Table("RDSResourceType").Set(new { AccessKey = configuration.GetString("coscine/global/rds/ecs-rwth/rds/object_user_name") }).AllRows();
            Update.Table("RDSResourceType").Set(new { SecretKey = configuration.GetString("coscine/global/rds/ecs-rwth/rds/object_user_secretkey") }).AllRows();
            Update.Table("RDSResourceType").Set(new { Endpoint = configuration.GetString("coscine/global/rds/ecs-rwth/rds/s3_endpoint") }).AllRows();

            // Make NotNullable
            Alter.Table("RDSResourceType")
                .AlterColumn("AccessKey").AsString(200).NotNullable()
                .AlterColumn("SecretKey").AsString(200).NotNullable()
                .AlterColumn("Endpoint").AsString(200).NotNullable();

            // Add resourcetypes
            Insert.IntoTable("ResourceTypes").Row(new { DisplayName = "rdss3ude" });
            Insert.IntoTable("ResourceTypes").Row(new { DisplayName = "rdsude" });

            // Add quotas
            Execute.EmbeddedScript("Migration202109161043UdeResources_up.sql");
        }
    }
}
