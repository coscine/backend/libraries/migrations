﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201907011352)]
    public class Migration201907011352ProjectApi : FluentMigrator.Migration
    { 
        public override void Down()
        {
            #region Foreign Keys
            Delete.ForeignKey()
                .FromTable("Resources").ForeignColumn("TypeId")
                .ToTable("ResourceTypes").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("SubProjects").ForeignColumn("ProjectId")
                .ToTable("Projects").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("SubProjects").ForeignColumn("SubProjectId")
                .ToTable("Projects").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("ProjectResource").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("ProjectResource").ForeignColumn("ProjectId")
                .ToTable("Projects").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("ProjectRoles").ForeignColumn("ProjectId")
                .ToTable("Projects").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("ProjectRoles").ForeignColumn("UserId")
                .ToTable("Users").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("ProjectRoles").ForeignColumn("RoleId")
                .ToTable("Roles").PrimaryColumn("Id");
            #endregion

            #region Tables
            Delete.Table("ResourceTypes");
            Delete.Table("Projects");
            Delete.Table("Users");
            Delete.Table("Roles");
            Delete.Table("Resources");
            Delete.Table("SubProjects");
            Delete.Table("ProjectResource");
            Delete.Table("ProjectRoles");
            #endregion
        }

        public override void Up()
        {
            #region Independent Tables
            Create.Table("ResourceTypes")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisplayName").AsString(50).NotNullable();

            Create.Table("Projects")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisplayName").AsString(200).NotNullable()
                .WithColumn("Description").AsString(1000).NotNullable()
                .WithColumn("Organization").AsString(50).NotNullable();


            Create.Table("Users")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid);

            Create.Table("Roles")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisplayName").AsString(50).NotNullable()
                .WithColumn("Description").AsString(50).NotNullable();

            Insert.IntoTable("Roles").Row(new { DisplayName = "Owner", Description = "Owner of the project." });
            Insert.IntoTable("Roles").Row(new { DisplayName = "Member", Description = "Member of the project." });
            #endregion

            #region Resources
            Create.Table("Resources")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("Url").AsString(200).NotNullable()
                .WithColumn("ExternalId").AsString(200).NotNullable()
                .WithColumn("TypeId").AsGuid().NotNullable();

            Create.ForeignKey()
                .FromTable("Resources").ForeignColumn("TypeId")
                .ToTable("ResourceTypes").PrimaryColumn("Id");
            #endregion

            #region SubProjects
            Create.Table("SubProjects")
                .WithColumn("RelationId").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("ProjectId").AsGuid().NotNullable()
                .WithColumn("SubProjectId").AsGuid().NotNullable();

            Create.ForeignKey()
                .FromTable("SubProjects").ForeignColumn("ProjectId")
                .ToTable("Projects").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("SubProjects").ForeignColumn("SubProjectId")
                .ToTable("Projects").PrimaryColumn("Id");
            #endregion

            #region ProjectResource
            Create.Table("ProjectResource")
                .WithColumn("RelationId").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("ResourceId").AsGuid().NotNullable()
                .WithColumn("ProjectId").AsGuid().NotNullable();

            Create.ForeignKey()
                .FromTable("ProjectResource").ForeignColumn("ResourceId")
                .ToTable("Resources").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("ProjectResource").ForeignColumn("ProjectId")
                .ToTable("Projects").PrimaryColumn("Id");
            #endregion

            #region ProjectRoles
            Create.Table("ProjectRoles")
                .WithColumn("RelationId").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("ProjectId").AsGuid().NotNullable()
                .WithColumn("UserId").AsGuid().NotNullable()
                .WithColumn("RoleId").AsGuid().NotNullable();

            Create.ForeignKey()
                .FromTable("ProjectRoles").ForeignColumn("ProjectId")
                .ToTable("Projects").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("ProjectRoles").ForeignColumn("UserId")
                .ToTable("Users").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("ProjectRoles").ForeignColumn("RoleId")
                .ToTable("Roles").PrimaryColumn("Id");
            #endregion
        }
    }
}
