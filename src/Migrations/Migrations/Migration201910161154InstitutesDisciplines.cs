﻿using FluentMigrator;
using System;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201910161154)]
    public class Migration201910161154InstitutesDisciplines : FluentMigrator.Migration
    {
        public override void Down()
        {
            MigrationsHelpers.HandleInstitute((obj) => Delete.FromTable("Institutes").Row(obj));
            MigrationsHelpers.HandleDiscipline((obj) => Delete.FromTable("Disciplines").Row(obj));
        }

        public override void Up()
        {
            MigrationsHelpers.HandleInstitute((obj) => Insert.IntoTable("Institutes").Row(obj));
            MigrationsHelpers.HandleDiscipline((obj) => Insert.IntoTable("Disciplines").Row(obj));
        }

        internal void HandleInstitute(Action<object> action)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "Coscine.Database.Migration.Assets.Institutes.csv";
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (var reader = new StreamReader(stream))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (line.Contains(","))
                    {
                        var values = line.Split(',');
                        if (values[0].Trim() != "" && values[1].Trim() != "")
                        {
                            action.Invoke(new { IKZ = values[0].Trim(), DisplayName = values[1].Trim() });
                        }
                    }
                }
            }
        }
    }
}
