﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201909160919)]
    public class Migration201909160919UserEnhancement : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Column("Givenname").FromTable("Users");
            Delete.Column("Surname").FromTable("Users");
        }

        public override void Up()
        {
            Alter.Table("Users").AddColumn("Givenname").AsString(200).Nullable();
            Alter.Table("Users").AddColumn("Surname").AsString(200).Nullable();
        }
    }
}
