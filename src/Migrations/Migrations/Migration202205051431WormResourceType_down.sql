﻿DECLARE @rdsS3wormId AS uniqueidentifier
SELECT @rdsS3wormId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'rdss3worm';

DELETE FROM [dbo].[ProjectQuotas]
      WHERE [ResourceTypeId] = @rdsS3wormId