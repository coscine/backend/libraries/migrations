﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202002041409)]
    public class Migration202002041409ProjectDeletedColumn : FluentMigrator.Migration
    {
        public override void Down()
        {    
            Delete.Column("Deleted")
                .FromTable("Projects");
        }

        public override void Up()
        {
            Alter.Table("Projects")
                .AddColumn("Deleted")
                .AsBoolean()
                .NotNullable()
                .WithDefaultValue(false);
        }
    }
}
