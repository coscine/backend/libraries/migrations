﻿DECLARE @gitlabId AS uniqueidentifier
SELECT @gitlabId = [Id]
FROM [dbo].[ResourceTypes]
WHERE [DisplayName] = 'gitlab';

INSERT INTO [ProjectQuotas]
           ([ProjectId]
           ,[ResourceTypeId]
           ,[Quota]
           ,[MaxQuota])

SELECT [Id], @gitlabId, 0, 0
FROM [Projects];

-- delete old gitlab entries from resources
DELETE FROM [dbo].[Resources]
WHERE [TypeId] = @gitlabId;
