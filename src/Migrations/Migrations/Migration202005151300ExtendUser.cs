﻿using FluentMigrator;
namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202005151300)]
    public class Migration202005151300ExtendUser : FluentMigrator.Migration
    {
        public override void Down()
        {
            #region Foreign Keys
            Delete.ForeignKey()
                .FromTable("Users").ForeignColumn("TitleId")
                .ToTable("Titles").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("Users").ForeignColumn("LanguageId")
                .ToTable("Languages").PrimaryColumn("Id");

            Delete.ForeignKey()
                .FromTable("UserDisciplines").ForeignColumn("DisciplineId")
                .ToTable("Disciplines").PrimaryColumn("Id");
            Delete.ForeignKey()
                .FromTable("UserDisciplines").ForeignColumn("UserId")
                .ToTable("Users").PrimaryColumn("Id");
            #endregion

            Delete.Column("TitleId").FromTable("Users");
            Delete.Column("LanguageId").FromTable("Users");
            Delete.Column("Institute").FromTable("Users");

            Delete.Table("Titles");
            Delete.Table("Languages");
            Delete.Table("UserDisciplines");
        }

        public override void Up()
        {
            Create.Table("Titles")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisplayName").AsString(50).NotNullable();

            Insert.IntoTable("Titles").Row(new { DisplayName = "Prof." });
            Insert.IntoTable("Titles").Row(new { DisplayName = "Dr." });

            Create.Table("Languages")
                .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisplayName").AsString(50).NotNullable()
                .WithColumn("Abbreviation").AsString(50).NotNullable();

            Insert.IntoTable("Languages").Row(new { DisplayName = "English", Abbreviation = "en" });
            Insert.IntoTable("Languages").Row(new { DisplayName = "Deutsch", Abbreviation = "de" });

            Create.Table("UserDisciplines")
                .WithColumn("RelationId").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
                .WithColumn("DisciplineId").AsGuid().NotNullable()
                .WithColumn("UserId").AsGuid().NotNullable();

            Alter.Table("Users").AddColumn("TitleId").AsGuid().Nullable();
            Alter.Table("Users").AddColumn("LanguageId").AsGuid().Nullable();
            Alter.Table("Users").AddColumn("Institute").AsString(200).Nullable();

            #region Foreign Keys
            Create.ForeignKey()
                .FromTable("Users").ForeignColumn("TitleId")
                .ToTable("Titles").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("Users").ForeignColumn("LanguageId")
                .ToTable("Languages").PrimaryColumn("Id");

            Create.ForeignKey()
                .FromTable("UserDisciplines").ForeignColumn("DisciplineId")
                .ToTable("Disciplines").PrimaryColumn("Id");
            Create.ForeignKey()
                .FromTable("UserDisciplines").ForeignColumn("UserId")
                .ToTable("Users").PrimaryColumn("Id");
            #endregion
        }
    }
}
