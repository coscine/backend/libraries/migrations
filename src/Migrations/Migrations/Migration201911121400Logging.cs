﻿using FluentMigrator;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(201911121400)]
    public class Migration201911121400Logging : FluentMigrator.Migration
    {
        public override void Down()
        {
            Delete.Table("Log");
        }

        public override void Up()
        {
            Create.Table("Log")
            .WithColumn("Id").AsGuid().PrimaryKey().WithDefault(SystemMethods.NewGuid)
            .WithColumn("ServerTimestamp").AsDateTime().WithDefault(SystemMethods.CurrentDateTime).Nullable()
            .WithColumn("ClientTimestamp").AsDateTime().WithDefault(SystemMethods.CurrentDateTime).Nullable()
            .WithColumn("LogLevel").AsString(20).Nullable()
            .WithColumn("Message").AsString(500).Nullable()
            .WithColumn("Stacktrace").AsString(8000).Nullable()
            .WithColumn("UserId").AsGuid().Nullable()
            .WithColumn("URI").AsString(500).Nullable()
            .WithColumn("Server").AsString(200).Nullable()
            .WithColumn("CorrolationId").AsGuid().Nullable()
            .WithColumn("Status").AsString(200).Nullable()
            .WithColumn("Source").AsString(200).Nullable();
            
        }

    }
}