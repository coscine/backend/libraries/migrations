﻿using FluentMigrator;

namespace Coscine.Migrations.Migrations
{
    //yyyymmddhhmm
    [Migration(202301161050)]
    public class Migration202301161050DropKpiTable : Migration
    {
        public override void Down()
        {
            Create.Table("Kpi");
        }

        public override void Up()
        {
            Delete.Table("Kpi");
        }
    }
}