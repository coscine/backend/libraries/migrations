﻿using Coscine.Configuration;
using Coscine.Database.Helpers;
using Coscine.Migrations;
using System;

namespace Coscine.Migrator
{
    public class Program
    {
        private const string Instruction =
            "possible actions: \n" +
            "--migrate_up\n" +
            "--roll_back <number of roll back steps>";

        public static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                PrintHelp();
            }
            else
            {
                var settings = new ConfigurationConnectionSettings() { Configuration = new ConsulConfiguration() };
                settings.LoadSettingsFromConfiguration();
                var migrator = new CoscineMigrations();
                var helper = new DatabaseMasterHelper();

                migrator.ConnectionSettings = settings;
                helper.ConnectionSettings = settings;

                if (string.IsNullOrWhiteSpace(migrator.ConnectionSettings.Database))
                {
                    Console.WriteLine("The Consul value for \"coscine/global/db_name\" is invalid.");
                    return;
                }

                switch (args[0])
                {
                    case "--migrate_up":
                        try
                        {
                            helper.EnsureDatabase(migrator.ConnectionSettings.Database);
                            migrator.MigrateUp();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Something went wrong trying to migrate up.");
                            Console.WriteLine("Error Log: " + e);
                        }
                        break;

                    case "--roll_back":
                        if (args.Length < 2)
                        {
                            Console.WriteLine("Invalid number of arguments provided for --roll_back (2).");
                            break;
                        }

                        if (int.TryParse(args[1], out var steps))
                        {
                            try
                            {
                                migrator.RollBack(steps);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Something went wrong trying to roll back.");
                                Console.WriteLine("Error Message: " + e);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Argument missing or invalid. Please enter the number of steps to roll back!");
                        }

                        break;

                    default:
                        Console.WriteLine("An invalid input was provided.");
                        PrintHelp();
                        break;
                }
            }
        }

        private static void PrintHelp()
        {
            Console.WriteLine("These are the possible actions:");
            Console.WriteLine(Instruction);
        }
    }
}