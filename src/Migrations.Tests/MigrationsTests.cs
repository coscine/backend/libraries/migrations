﻿using Coscine.Configuration;
using Coscine.Database.Helpers;
using Coscine.Migrations;
using NUnit.Framework;
using System;

namespace Migrations.Tests
{
    [TestFixture]
    public class MigrationsTests
    {

        private CoscineMigrations _migrator;
        private DatabaseMasterHelper _helper;
        private string _databaseName;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _databaseName = $"Coscine_Migrations.Tests_{Guid.NewGuid()}";

            _migrator = new CoscineMigrations();
            var settings = new ConfigurationConnectionSettings() { Configuration = new ConsulConfiguration() };

            settings.LoadSettingsFromConfiguration();
            _migrator.ConnectionSettings = settings;
            _migrator.ConnectionSettings.Database = _databaseName;

            _helper = new DatabaseMasterHelper
            {
                ConnectionSettings = settings
            };

        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            if (_helper.DatabaseExists(_databaseName))
            {
                _helper.KillConnectionsToDatabase(_databaseName);
                _helper.DropDatabase(_databaseName);
            }
        }

        [Test]
        public void MigrationTest()
        {
            _helper.EnsureDatabase(_databaseName);
            _migrator.MigrateUp();
            _helper.KillConnectionsToDatabase(_databaseName);
            _helper.DropDatabase(_databaseName);
        }
    }
}
